import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Hr hr = new Hr();
        hr.hireEmployee();

        Tech tech = new Tech();
        tech.hireEmployee();

        Product product = new Product();
        product.hireEmployee();

        // user apply job
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your mark to apply job:");
        int mark = scanner.nextInt();
        Job job = new Job();
        try {
            job.applyJob(mark);
        } catch (Exception e) {
            System.out.println("user is not eligible.");
        }
    }
}

class Job extends Exception {
    void applyJob(int mark) throws Exception {
        if (mark < 71) {
            Exception exception = new Exception();
            throw exception;
        } else {
            System.out.println("user is eligible.");
        }
    }
}

interface HrTeam {
    void hireEmployee();
}

class Hr implements HrTeam {

    @Override
    public void hireEmployee() {
        System.out.println("HR Department");
    }
}

class Tech implements HrTeam {
    @Override
    public void hireEmployee() {
        System.out.println("Tech Department");
    }
}

class Product implements HrTeam {
    @Override
    public void hireEmployee() {
        System.out.println("Product Department");
    }
}